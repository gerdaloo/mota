from django.contrib import admin
from .models import Ostan, Shahr
# Register your models here.

admin.site.register(Ostan)
admin.site.register(Shahr)
