from django.db import models

# Create your models here.
class Ostan(models.Model):
    name = models.CharField(max_length=70, verbose_name='نام استان', default="انتخاب نشده")
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'استان'
        verbose_name_plural = 'استانها'


class Shahr(models.Model):
    ostan = models.ForeignKey(Ostan,on_delete=models.CASCADE,verbose_name='استان')
    name = models.CharField(max_length=70, verbose_name='نام شهر', default="انتخاب نشده")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'شهر'
        verbose_name_plural = 'شهرها '
