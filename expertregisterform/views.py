from django.shortcuts import render
from .forms import ExpertForm
# Create your views here.

def index(request):
    form = ExpertForm
    return render(request, 'index.html',{'form':form})