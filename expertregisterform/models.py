from django.db import models
from locations.models import Ostan,Shahr
from smart_selects.db_fields import ChainedForeignKey
from django_jalali.db import models as jmodels
# Create your models here.

class ExpertRegisterForm(models.Model):
    name = models.CharField(max_length=50, verbose_name='نام')
    last_name = models.CharField(max_length=100, verbose_name='نام خانوادگی')
    fathers_name = models.CharField(max_length=50, verbose_name='نام پدر')
    birthday = jmodels.jDateField(verbose_name='تاریخ تولد')
    gender = models.CharField(max_length=6, choices=(('male','مذکر'),('female','مونث')), verbose_name='جنسیت')
    code_meli = models.IntegerField(verbose_name='کد ملی')
    code_shenasnameh = models.IntegerField(verbose_name='شماره شناسنامه')
    email = models.CharField(max_length=200, verbose_name='ایمیل')
    mobile = models.IntegerField(verbose_name='شماره موبایل')
    phone = models.IntegerField(verbose_name='شماره تلفن')
    ostan = models.ForeignKey(Ostan, on_delete=models.SET_DEFAULT, related_name='ostan', default="انتخاب نشده",verbose_name='استان')
    shahr = ChainedForeignKey(Shahr,chained_field="ostan",chained_model_field="ostan",auto_choose=True, sort=True,verbose_name='شهر')
    address = models.TextField(verbose_name='آدرس محل سکونت')
    code_posti = models.IntegerField(verbose_name='کد پستی')
    avatar = models.ImageField(verbose_name='تصویر پرسنلی')
    cartmeli = models.ImageField(verbose_name='کارت ملی')
    shensnameh = models.ImageField(verbose_name='شناسنامه')
    cv = models.ImageField(verbose_name='رزومه')
    madrak = models.ImageField(verbose_name='مدرک تحصیلی')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'کارشناس'
        verbose_name_plural = 'کارشناسان'


        
       
        
        
        
       