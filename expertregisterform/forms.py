from django import forms
from .models import ExpertRegisterForm

class ExpertForm(forms.ModelForm):
    class Meta:
        model = ExpertRegisterForm
        fields = "__all__"
        widgets = {
            'birthday': forms.DateInput(attrs={'id':'datepicker'}),
        }