from django.apps import AppConfig


class ExpertregisterformConfig(AppConfig):
    name = 'expertregisterform'
    verbose_name = 'فرم ثبت نام کارشناسان'
